cmake_minimum_required(VERSION 3.8)
project(nanotubes)

set(CMAKE_CXX_STANDARD 17)

add_subdirectory(xgl)

add_library(nanotubes STATIC
	include/nanotubes/constants.hpp
	include/nanotubes/color.hpp
	include/nanotubes/shader_base.hpp
	include/nanotubes/ball_renderer.hpp
	include/nanotubes/stick_renderer.hpp
	include/nanotubes/nanotube_renderer.hpp
	include/nanotubes/mesh_renderer.hpp
	include/nanotubes/line_renderer.hpp
	include/nanotubes/camera.hpp
	include/nanotubes/nanotube.hpp
	include/nanotubes/util.hpp
	include/nanotubes/application.hpp
	include/nanotubes/nogui_application.hpp
	source/nanotubes/ball_renderer.cpp
	source/nanotubes/stick_renderer.cpp
	source/nanotubes/line_renderer.cpp
	source/nanotubes/mesh_renderer.cpp
	source/nanotubes/camera.cpp
	source/nanotubes/nanotube.cpp
	source/nanotubes/util.cpp
)
target_link_libraries(nanotubes PUBLIC xgl xsdl)
target_include_directories(nanotubes PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

add_executable(nanotube_demo source/nanotube_demo.cpp)
target_link_libraries(nanotube_demo nanotubes)

add_executable(nanotube_isosurface
	source/nanotube_isosurface.cpp
	source/nanotube_isosurface_generate.cpp
	source/nanotube_isosurface_view.cpp
)
target_link_libraries(nanotube_isosurface nanotubes)

add_executable(nanotube_md source/nanotube_md.cpp)
target_link_libraries(nanotube_md nanotubes)

#!/usr/bin/env python3

import sys
import os

import numpy as np
import matplotlib.pyplot as plt

data = {}

root = sys.argv[1]

for nanotube in os.listdir(root):
    if nanotube not in data:
        data[nanotube] = []

    for item in os.listdir(os.path.join(root, nanotube)):
        item_data = None
        with open(os.path.join(root, nanotube, item), 'rt') as f:
            for line in f:
                l = line.split(' ')
                if l[1] == 'internal':
                    item_data = float(l[3])
        data[nanotube].append(item_data)

min_internal = []

for nanotube in data.keys():
    min_internal.append((min(data[nanotube]), max(data[nanotube]), nanotube))

for x in sorted(min_internal, key=lambda x: x[0]):
    print(x)

Emin = [a[0] for a in min_internal if a[0] < 1e-4]
Emax = [a[1] for a in min_internal if a[0] < 1e-4]

for i in range(len(Emin)):
    plt.plot((Emin[i], Emin[i]), (Emin[i], Emax[i]), 'o-b')
plt.show()

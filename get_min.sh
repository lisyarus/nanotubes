#!/usr/bin/env bash

echo n m r min_d

for n in $(seq 8 15); do
  for m in $(seq 0 $n); do
    r=$(python3 -c "import math;print(0.5*math.sqrt(3)*1.44/math.pi*math.sqrt($n*$n+$n*$m+$m*$m))")
    min_d=$(./release/nanotube_isosurface generate $n $m 50 20.0 20.0 0.2 /dev/null | grep minima | sed -e "s/^.*distance: \(.*\)$/\1/")
    echo $n $m $r $min_d
  done
done

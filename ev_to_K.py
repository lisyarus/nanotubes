#!/usr/bin/env python3

import sys

for arg in sys.argv[1:]:
    eV = float(arg)
    print(eV * 1.6021766208 / 1.38064852 / 3 * 1e4)


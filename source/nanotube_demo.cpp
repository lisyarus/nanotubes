#include <nanotubes/application.hpp>
#include <nanotubes/nanotube_renderer.hpp>

#include <nanotubes/constants.hpp>
#include <nanotubes/nanotube.hpp>
#include <nanotubes/util.hpp>

#include <xgl/state/enable.hpp>

#include <util/autoname.hpp>
#include <util/to_string.hpp>

#include <iostream>

int main (int argc, char ** argv) try
{
	if (argc != 4)
	{
		std::cout << "Usage: " << argv[0] << " [n] [m] [l]\n";
		return 1;
	}

	int const nanotube_n = util::from_string<int>(argv[1]);
	int const nanotube_m = util::from_string<int>(argv[2]);
	int const nanotube_l = util::from_string<int>(argv[3]);

	auto nanotube = nanotube_graph(nanotube_n, nanotube_m, nanotube_l, carbon_carbon_bond_length);
	center_z(nanotube.vertices);

	application app;

	nanotube_renderer nr(12);
	nr.load(nanotube);

	app.run([&]
	{
		glClearColor(0.7f, 0.8f, 0.9f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		{
			xgl::scope::enable autoname(GL_DEPTH_TEST);
			xgl::scope::enable autoname(GL_CULL_FACE);

			app.setup_shader(nr.balls().shader());
			nr.balls().render();

			app.setup_shader(nr.sticks().shader());
			nr.sticks().render();
		}
	});
}
catch (std::exception const & e)
{
	std::cerr << e.what() << std::endl;
	return 1;
}

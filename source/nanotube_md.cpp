#include <nanotubes/constants.hpp>
#include <nanotubes/application.hpp>
#include <nanotubes/nogui_application.hpp>
#include <nanotubes/nanotube.hpp>
#include <nanotubes/util.hpp>

#include <xgl/color.hpp>

#include <geom/operations/point.hpp>
#include <geom/random/vector.hpp>
#include <geom/math.hpp>

#include <util/to_string.hpp>

#include <iomanip>

#include <chrono>

template <typename Scalar, typename App>
int main2 (int argc, char ** argv, App & app)
{
	auto print_usage = [&]
	{
		std::cerr << "Usage: " << argv[0] << " free  <length>\n";
		std::cerr << "       " << argv[0] << " bound <length> <n> <m> <l> <x-shift>\n";
	};

	if (argc != 3 && argc != 7)
	{
		print_usage();
		return -1;
	}

	std::string const mode = argv[1];

	if (mode == "free" && argc != 3)
	{
		print_usage();
		return -1;
	}
	else if (mode == "bound" && argc != 7)
	{
		print_usage();
		return -1;
	}
	else if (mode != "free" && mode != "bound")
	{
		std::cerr << "Unknown mode \"" << mode << "\"\n";
		print_usage();
		return -1;
	}

	bool const bound = (mode == "bound");

	int const polyethylene_length = util::from_string<int>(argv[2]);

	using point = geom::point_3<Scalar>;
	using vector = geom::vector_3<Scalar>;

	geom::triangulation<point, 1> nanotube;

	if (bound)
	{
		int const nanotube_n = util::from_string<int>(argv[3]);
		int const nanotube_m = util::from_string<int>(argv[4]);
		int const nanotube_l = util::from_string<int>(argv[5]);
		nanotube = nanotube_graph<Scalar>(nanotube_n, nanotube_m, nanotube_l, carbon_carbon_bond_length);
		center_z(nanotube.vertices);
	}

	float const shift = bound ? util::from_string<Scalar>(argv[6]) : 0;

	typename App::nanotube_renderer nr(12);
	nr.load(nanotube);

	std::vector<point> atoms;
	{
		Scalar const dx =               polyethylene_r0 * std::sin(polyethylene_theta0 * Scalar(0.5));
		Scalar const dy = Scalar(0.5) * polyethylene_r0 * std::cos(polyethylene_theta0 * Scalar(0.5));
		for (int i = 0; i < polyethylene_length; ++i)
		{
			atoms.push_back({ shift, (i % 2 == 0) ? dy : -dy, (i - polyethylene_length * Scalar(0.5)) * dx });
		}
	}

	auto lj_single = [](point const & p1, point const & p2)
	{
		Scalar const r_2 = geom::distance_sqr(p1, p2);
		Scalar const sigma_over_r_2 = (carbon_carbon_lj_sigma * carbon_carbon_lj_sigma) / r_2;

		Scalar const sigma_over_r_6 = sigma_over_r_2 * sigma_over_r_2 * sigma_over_r_2;

		return 4 * carbon_carbon_lj_epsilon * (sigma_over_r_6 * sigma_over_r_6 - sigma_over_r_6);
	};

	auto internal_r_energy = [&](std::vector<point> const & atoms)
	{
		Scalar e = 0.f; // eV

		for (std::size_t i = 0; i + 1 < atoms.size(); ++i)
		{
			Scalar const r = geom::distance(atoms[i], atoms[i+1]);
			e += polyethylene_kr * geom::sqr(r - polyethylene_r0);
		}

		return e;
	};

	auto internal_theta_energy = [&](std::vector<point> const & atoms)
	{
		Scalar e = 0.f; // eV

		for (std::size_t i = 0; i + 2 < atoms.size(); ++i)
		{
			auto const v0 = atoms[i  ] - atoms[i+1];
			auto const v1 = atoms[i+2] - atoms[i+1];
			Scalar const theta = std::acos((v0 * v1) / norm(v0) / norm(v1));
			e += polyethylene_ktheta * geom::sqr(theta - polyethylene_theta0);
		}

		return e;
	};

	auto internal_energy = [&](std::vector<point> const & atoms)
	{
		return internal_r_energy(atoms) + internal_theta_energy(atoms);
	};

	auto energy = [&](std::vector<point> const & atoms)
	{
		Scalar e = 0.f; // eV

		if (bound) for (auto const & p : atoms)
		{
			for (auto const & q : nanotube.vertices)
			{
				e += lj_single(p, q);
			}
		}

		return e + internal_energy(atoms);
	};

	auto average_x = [](std::vector<point> const & atoms)
	{
		Scalar x = 0.f; // angstrom

		for (auto const & p : atoms)
		{
			x += p[0];
		}

		return x / atoms.size();
	};

	Scalar E = energy(atoms); // eV

	typename App::line_renderer lr(GL_LINE_STRIP);
	lr.shader().color(xgl::color::red);

	typename App::nanotube_renderer pr(12);

	std::default_random_engine rng(std::chrono::system_clock::now().time_since_epoch().count());

	Scalar step = 0.25;
	int bad_iteration_count = 0;
	bool finished = false;

	auto monte_carlo = [&]
	{
		geom::random::uniform_sphere_vector_distribution<vector> rand_d(step);

		bool bad_iteration = true;

		for (std::size_t i = 0; i < atoms.size(); ++i)
		{
			auto prev = atoms[i];
			atoms[i] += rand_d(rng);
			Scalar new_E = energy(atoms);
			if (new_E < E)
			{
				E = new_E;
				bad_iteration = false;
			}
			else
				atoms[i] = prev;
		}

		return !bad_iteration;
	};

	auto gradient_descent = [&]
	{
		std::vector<vector> gradients(atoms.size(), vector::zero());

		for (std::size_t i = 0; i + 1 < atoms.size(); ++i)
		{
			auto const r = atoms[i] - atoms[i+1];
			auto const g = 2.f * polyethylene_kr * (1 - polyethylene_r0 / geom::norm(r)) * r;
			gradients[i  ] += g;
			gradients[i+1] -= g;
		}

		for (std::size_t i = 0; i + 2 < atoms.size(); ++i)
		{
			auto const r01 = atoms[i  ] - atoms[i+1];
			auto const r21 = atoms[i+2] - atoms[i+1];

			Scalar const a = r01 * r21;
			Scalar const b = 1 / (geom::norm(r01) * geom::norm(r21));

			Scalar const theta = std::acos((r01 * r21) * b);

			Scalar const c = - 2 * polyethylene_ktheta * (theta - polyethylene_theta0) / std::sqrt(1 - a*a*b*b);

			auto const g0 = c * b * (r21 - a * r01 / geom::norm_sqr(r01));
			auto const g2 = c * b * (r01 - a * r21 / geom::norm_sqr(r21));

			gradients[i  ] += g0;
			gradients[i+1] -= g0 + g2;
			gradients[i+2] += g2;
		}

		Scalar max_norm = 0.f;

		for (std::size_t i = 0; i < atoms.size(); ++i)
		{
			auto n = geom::norm(gradients[i]);
			if (n > max_norm)
				max_norm = n;
		}

		for (std::size_t i = 0; i + 1 < atoms.size(); ++i)
		{
			atoms[i] -= gradients[i] / max_norm * step;
		}

		return false;
	};

	auto linear_monte_carlo = [&]
	{
		geom::random::uniform_sphere_vector_distribution<vector> rand_d;

		std::vector<point> old_atoms = atoms;
		std::vector<point> new_atoms(atoms.size());
		std::vector<point> cur_atoms(atoms.size());

		for (std::size_t i = 0; i < atoms.size(); ++i)
			new_atoms[i] = old_atoms[i] + rand_d(rng) * step;

		for (int p = 0; p < 2; ++p)
		{
			bool found = false;

			Scalar E = energy(old_atoms);

			int const N = 1024;

			for (int it = 1; it <= N; ++it)
			{
				Scalar const t = Scalar(it) / N;

				for (std::size_t i = 0; i < old_atoms.size(); ++i)
					cur_atoms[i] = old_atoms[i] + (new_atoms[i] - old_atoms[i]) * t;

				Scalar E_cur = energy(cur_atoms);

				if (E_cur < E)
				{
					Scalar const tprev = Scalar(it-1) / N;

					for (std::size_t i = 0; i < old_atoms.size(); ++i)
						old_atoms[i] = old_atoms[i] + (new_atoms[i] - old_atoms[i]) * tprev;

					new_atoms = cur_atoms;

					found = true;
					break;
				}
			}

			if (!found)
				return false;
		}

		atoms = new_atoms;

		return true;
	};

	std::size_t barrier_tries = 0;
	Scalar min_barrier = std::numeric_limits<Scalar>::infinity();

	app.run([&]
	{
		if (!finished)
		{
			for (int iteration = 0; iteration < 1; ++iteration)
			{
				auto const & method = monte_carlo;

				if (!method())
					++bad_iteration_count;
				else
					bad_iteration_count = 0;

				app.debug()
					<< "Step: " << step << ", total energy: " << std::setprecision(15) << energy(atoms) << ", internal energy: " << internal_energy(atoms) << " eV";
//					<< ", including " << internal_r_energy(atoms) << " eV for r and "
//					<< internal_theta_energy(atoms) << " eV for theta";

				app.debug() << ", average x: " << average_x(atoms) << " Å\n";
			}

			if (bad_iteration_count >= (1 << 5))
			{
				step *= 0.75;
				if (step < 1.e-6)
				{
					finished = true;
				}
				else
				{
					app.debug() << "Reducing step to " << step << "\n";
					bad_iteration_count = 0;
				}
			}
		}
		else
		{
			geom::triangulation<point, 1> polyethylene;

			polyethylene.vertices = atoms;
			for (std::size_t i = 1; i < atoms.size(); ++i)
				polyethylene.simplices.push_back({i-1, i});

			pr.load(polyethylene, xgl::color::red);

			app.debug() << "Finished\n";
			app.stop();
		}

		if (false)
			if (barrier_tries < 10000)
		{
			Scalar length = 1e-3;

			geom::random::uniform_sphere_vector_distribution<vector> rand_d;

			std::vector<point> new_atoms(atoms.size());

			std::vector<point> cur_atoms(atoms.size());

			Scalar E = energy(atoms);

			bool barrier_found = false;

			while (!barrier_found)
			{
				for (std::size_t i = 0; i < atoms.size(); ++i)
					new_atoms[i] = atoms[i] + rand_d(rng) * length;
				Scalar E_prev = E;

				std::vector<Scalar> E_list;
				E_list.push_back(E);

				int N = 1024;

				for (int it = 1; it <= N; ++it)
				{
					Scalar const t = Scalar(it) / N;

					for (std::size_t i = 0; i < atoms.size(); ++i)
						cur_atoms[i] = atoms[i] + (new_atoms[i] - atoms[i]) * t;

					Scalar E_cur = energy(cur_atoms);
					E_list.push_back(E_cur);

					if (E_cur < E)
					{
						break;
					}

					if (E_cur < E_prev)
					{
						barrier_found = true;
						Scalar barrier = E_prev - E;

						app.debug() << "Found barrier at " << (length * t) << " Å (i = " << it << "), ΔE = " << barrier << "\n";

						if (min_barrier > barrier)
							min_barrier = barrier;

						app.debug() << "Energy list: ";
						for (auto e : E_list)
							app.debug() << std::setprecision(15) << e << ' ';
						app.debug() << "\n";

						break;
					}

					E_prev = E_cur;
				}
			}

			++barrier_tries;
		}
		else
		{
			app.debug() << "Finished\n";
			app.stop();
		}

		app.render(nr.balls());
		app.render(nr.sticks());

		if (!finished)
		{
			lr.load(atoms);
			app.render(lr);
		}
		else
		{
			app.render(pr.balls());
			app.render(pr.sticks());
		}
	});

	std::cout << "Final total energy: " << energy(atoms) << " eV\n";
	std::cout << "Estimated total energy barrier: " << min_barrier << " eV\n";
	std::cout << "Final internal energy: " << internal_energy(atoms) << " eV\n";
	std::cout << "Final r energy: " << internal_r_energy(atoms) << " eV\n";
	std::cout << "Final theta energy: " << internal_theta_energy(atoms) << " eV\n";
	std::cout << "Final average x: " << average_x(atoms) << " Å\n";
}

static bool ends_with (std::string const & str, std::string const & suffix)
{
	if (suffix.size() > str.size())
		return false;

	return str.substr(str.size() - suffix.size(), suffix.size()) == suffix;
}

int main (int argc, char ** argv) try
{
	if (ends_with(argv[0], "nanotube_md"))
	{
		application app;
		main2<float>(argc, argv, app);
	}
	else if (ends_with(argv[0], "nanotube_md_nogui"))
	{
		nogui_application app;
		main2<double>(argc, argv, app);
	}
}
catch (std::exception const & e)
{
	std::cerr << e.what() << std::endl;
	return 1;
}

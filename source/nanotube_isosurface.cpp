#include <iostream>

void run_generate (int argc, char ** argv);
void run_view (int argc, char ** argv);

int main (int argc, char ** argv) try
{
	auto show_usage = [argv]
	{
		std::cout << "Usage: " << argv[0] << " generate [n] [m] [l] [box_xy] [box_z] [cell] [data_output_path]\n";
		std::cout << "       " << argv[0] << " view [data_path] [isosurface_value_1] .. [isosurface_value_N]\n";
	};

	if (argc < 2)
	{
		show_usage();
		return 1;
	}

	std::string const mode = argv[1];

	if (mode == "generate")
	{
		if (argc != 9)
		{
			show_usage();
			return 1;
		}
		run_generate(argc, argv);
	}
	else if (mode == "view")
	{
		if (argc < 4)
		{
			show_usage();
			return 1;
		}
		run_view(argc, argv);
	}
	else
	{
		std::cout << "Mode not supported: " << mode << "\n";
		return 1;
	}
}
catch (std::exception const & e)
{
	std::cerr << e.what() << std::endl;
	return 1;
}

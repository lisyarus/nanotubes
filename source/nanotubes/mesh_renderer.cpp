#include <nanotubes/mesh_renderer.hpp>

#include <util/autoname.hpp>

static const char shader_vs[] =
R"(#version 330

uniform mat4 modelview;
uniform mat4 projection;

layout (location = 0) in vec4 position;
layout (location = 1) in vec3 normal;

out vec3 position_v;
out vec3 normal_v;

void main ( )
{
	vec4 position_world = modelview * position;
	gl_Position = projection * position_world;
	position_v = position_world.xyz;
	normal_v = (modelview * vec4(normal, 0.0)).xyz;
})";

static const char shader_fs[] =
R"(#version 330

uniform vec4 ambient_color;
uniform vec4 light1_color;
uniform vec4 light1_position;
uniform vec4 light2_color;
uniform vec4 light2_position;

uniform vec4 mesh_color;

in vec3 position_v;
in vec3 normal_v;

void main ( )
{
	float t1 = max(0.0, dot(normalize(normal_v), normalize(light1_position.xyz - position_v * light1_position.w)));
	float t2 = max(0.0, dot(normalize(normal_v), normalize(light2_position.xyz - position_v * light2_position.w)));

	if (t1 + t2 > 1.0)
	{
		t1 /= (t1 + t2);
		t2 /= (t1 + t2);
	}

	gl_FragColor = (ambient_color * (1.0 - t1 - t2) + light1_color * t1 + light2_color * t2) * mesh_color;
}

)";

xgl::program mesh_shader::make_program ( )
{
	return {
		xgl::vertex_shader(shader_vs),
		xgl::fragment_shader(shader_fs),
	};
}

void mesh_shader::modelview (geom::matrix_4x4f const & transform)
{
	program_["modelview"] = transform;
}

void mesh_shader::projection (geom::matrix_4x4f const & transform)
{
	program_["projection"] = transform;
}

void mesh_shader::ambient_color (geom::vector_4f const & color)
{
	program_["ambient_color"] = color;
}

void mesh_shader::light1_color (geom::vector_4f const & color)
{
	program_["light1_color"] = color;
}

void mesh_shader::light1_position (geom::vector_4f const & position)
{
	program_["light1_position"] = position;
}

void mesh_shader::light2_color (geom::vector_4f const & color)
{
	program_["light2_color"] = color;
}

void mesh_shader::light2_position (geom::vector_4f const & position)
{
	program_["light2_position"] = position;
}

void mesh_shader::mesh_color (geom::vector_4f const & color)
{
	program_["mesh_color"] = color;
}

mesh_renderer::mesh_renderer ( )
	: count_(0)
{
	setup_array();
}

void mesh_renderer::setup_array ( )
{
	xgl::scope::bind_array autoname(array_);

	xgl::scope::bind_buffer autoname(mesh_);
	array_[0].enable().data<geom::point_3f>(0, sizeof(mesh_point));
	array_[1].enable().data<geom::vector_3f>(sizeof(float) * 3, sizeof(mesh_point));
}

void mesh_renderer::load (mesh_point const * data, std::size_t count)
{
	mesh_.data(data, count);
	count_ = count;
}

void mesh_renderer::render ( )
{
	xgl::scope::bind_program autoname(shader_.program());
	array_.draw(GL_TRIANGLES, count_);
}

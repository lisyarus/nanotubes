#include <nanotubes/camera.hpp>

#include <geom/transform/translation.hpp>
#include <geom/transform/rotation.hpp>
#include <geom/transform/homogeneous.hpp>

#include <xsdl/event_poller.hpp>

camera::camera ( )
{
	a = 0.f;
	b = 0.f;
	d = 40.f;
}

geom::matrix_4x4f camera::matrix ( ) const
{
	return geom::matrix_4x4f::identity()
		* geom::translation<float, 3>({0.f, 0.f, -d}).homogeneous_matrix()
		* geom::homogeneous(geom::plane_rotation<float, 3>(1, 2, b).matrix())
		* geom::homogeneous(geom::plane_rotation<float, 3>(2, 0, a).matrix())
		;
}

geom::vector_3f camera::direction ( ) const
{
	return {
		- std::cos(b) * std::sin(a),
		std::sin(b),
		std::cos(b) * std::cos(a),
	};
}

void setup_controls (camera & cam, xsdl::event_poller_t & event_poller)
{
	event_poller.on_mouse_move([&cam, &event_poller](xsdl::window_t::id_t, geom::point_2i, geom::vector_2i delta){
		if (event_poller.middle_button_down())
		{
			cam.a += delta[0] * 0.01f;
			cam.b += delta[1] * 0.01f;
		}
	});

	event_poller.on_wheel([&cam](xsdl::window_t::id_t, double delta){
		cam.d *= std::pow(0.8f, delta);
	});
}

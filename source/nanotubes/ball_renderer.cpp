#include <nanotubes/ball_renderer.hpp>

#include <geom/math.hpp>

#include <util/autoname.hpp>

static const char shader_vs[] =
R"(#version 330

uniform mat4 modelview;
uniform mat4 projection;

layout (location = 0) in vec4 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 translation;
layout (location = 3) in float radius;
layout (location = 4) in vec4 color;

out vec3 position_v;
out vec3 normal_v;
out vec4 color_v;

void main ( )
{
	vec4 position_world = modelview * vec4(position.xyz * radius + translation, 1.0);
	gl_Position = projection * position_world;
	position_v = position_world.xyz;
	normal_v = (modelview * vec4(normal, 0.0)).xyz;
	color_v = color;
})";

static const char shader_fs[] =
R"(#version 330

uniform vec4 ambient_color;
uniform vec4 light1_color;
uniform vec4 light1_position;
uniform vec4 light2_color;
uniform vec4 light2_position;

in vec3 position_v;
in vec3 normal_v;
in vec4 color_v;

void main ( )
{
	float t1 = max(0.0, dot(normalize(normal_v), normalize(light1_position.xyz - position_v * light1_position.w)));
	float t2 = max(0.0, dot(normalize(normal_v), normalize(light2_position.xyz - position_v * light2_position.w)));

	if (t1 + t2 > 1.0)
	{
		t1 /= (t1 + t2);
		t2 /= (t1 + t2);
	}

	gl_FragColor = (ambient_color * (1.0 - t1 - t2) + light1_color * t1 + light2_color * t2) * color_v;
}

)";

xgl::program ball_shader::make_program ( )
{
	return {
		xgl::vertex_shader(shader_vs),
		xgl::fragment_shader(shader_fs),
	};
}

void ball_shader::modelview (geom::matrix_4x4f const & transform)
{
	program_["modelview"] = transform;
}

void ball_shader::projection (geom::matrix_4x4f const & transform)
{
	program_["projection"] = transform;
}

void ball_shader::ambient_color (geom::vector_4f const & color)
{
	program_["ambient_color"] = color;
}

void ball_shader::light1_color (geom::vector_4f const & color)
{
	program_["light1_color"] = color;
}

void ball_shader::light1_position (geom::vector_4f const & position)
{
	program_["light1_position"] = position;
}

void ball_shader::light2_color (geom::vector_4f const & color)
{
	program_["light2_color"] = color;
}

void ball_shader::light2_position (geom::vector_4f const & position)
{
	program_["light2_position"] = position;
}

ball_renderer::ball_renderer (int quality)
	: count_(0)
	, instance_count_(0)
{
	set_quality(quality);
	setup_array();
}

void ball_renderer::set_quality (int quality)
{
	std::vector<geom::vector_3f> points;
	std::vector<std::uint32_t> indices;

	auto push_triangle = [&](std::size_t i1, std::size_t i2, std::size_t i3)
	{
		indices.push_back(static_cast<std::uint32_t>(i1));
		indices.push_back(static_cast<std::uint32_t>(i2));
		indices.push_back(static_cast<std::uint32_t>(i3));
	};

	auto index_at = [quality](std::size_t i, std::size_t j)
	{
		return 2 + (j - 1) * (4 * quality) + (i % (4 * quality));
	};

	points.push_back({0.f, 0.f, -1.f});
	points.push_back({0.f, 0.f,  1.f});

	for (std::size_t j = 1; j < 2 * quality; ++j)
	{
		float const aj = (geom::pi_f * j * 0.5f) / quality;

		for (std::size_t i = 0; i < 4 * quality; ++i)
		{
			float const ai = (geom::pi_f * i * 0.5f) / quality;

			geom::vector_3f n
			{
				std::cos(ai) * std::sin(aj),
				std::sin(ai) * std::sin(aj),
				-std::cos(aj)
			};

			points.push_back(n);
		}
	}

	// bottom cap
	for (std::size_t i = 0; i < 4 * quality; ++i)
	{
		push_triangle(0, index_at(i + 1, 1), index_at(i, 1));
	}

	// top cap
	for (std::size_t i = 0; i < 4 * quality; ++i)
	{
		push_triangle(1, index_at(i, 2 * quality - 1), index_at(i + 1, 2 * quality - 1));
	}

	// middle strip
	for (std::size_t j = 1; j < 2 * quality - 1; ++j)
	{
		for (std::size_t i = 0; i < 4 * quality; ++i)
		{
			push_triangle(index_at(i, j), index_at(i + 1, j), index_at(i + 1, j + 1));
			push_triangle(index_at(i, j), index_at(i + 1, j + 1), index_at(i, j + 1));
		}
	}

	sphere_.data(points);
	indices_.data(indices);

	count_ = indices.size();
}

void ball_renderer::setup_array ( )
{
	xgl::scope::bind_array autoname(array_);

	{
		xgl::scope::bind_buffer autoname(sphere_);
		array_[0].enable().data<geom::vector_3f>(0);
		array_[1].enable().data<geom::vector_3f>(0);
	}

	{
		xgl::scope::bind_buffer autoname(balls_);
		array_[2].enable().data<geom::vector_3f>(0, sizeof(ball)).divisor(1);
		array_[3].enable().data<float>(sizeof(float) * 3, sizeof(ball)).divisor(1);
		array_[4].enable().data<geom::vector_4f>(sizeof(float) * 4, sizeof(ball)).divisor(1);
	}

	array_.indices(indices_);
}

void ball_renderer::load (ball const * data, std::size_t count)
{
	balls_.data(data, count);
	instance_count_ = count;
}

void ball_renderer::render ( )
{
	xgl::scope::bind_program autoname(shader_.program());
	xgl::scope::bind_array autoname(array_);
	glDrawElementsInstanced(GL_TRIANGLES, count_, GL_UNSIGNED_INT, nullptr, instance_count_);
}

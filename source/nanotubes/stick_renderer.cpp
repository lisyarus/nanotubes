#include <nanotubes/stick_renderer.hpp>

#include <geom/math.hpp>

#include <util/autoname.hpp>

static const char shader_vs[] =
R"(#version 330

uniform mat4 modelview;
uniform mat4 projection;

layout (location = 0) in vec4 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 translation0;
layout (location = 3) in vec3 translation1;
layout (location = 4) in float radius0;
layout (location = 5) in float radius1;
layout (location = 6) in vec4 color0;
layout (location = 7) in vec4 color1;

out vec3 position_v;
out vec3 normal_v;
out vec4 color_v;

vec3 ort (vec3 v)
{
	if (abs(v.x) > 0.5)
		return vec3(v.y, -v.x, 0.0);
	else
		return vec3(0.0, v.z, -v.y);
}

void main ( )
{
	vec3 ez = translation1 - translation0;
	vec3 nz = normalize(ez);
	vec3 ex = ort(nz);
	vec3 ey = cross(nz, ex);

	float radius = mix(radius0, radius1, position.z);

	vec3 p = translation0 + ez * position.z + (ex * position.x + ey * position.y) * radius;
	vec4 position_world = modelview * vec4(p, 1.0);
	gl_Position = projection * position_world;
	position_v = position_world.xyz;
	normal_v = (modelview * vec4(ex * position.x + ey * position.y, 0.0)).xyz;
	color_v = mix(color0, color1, position.z);
})";

static const char shader_fs[] =
R"(#version 330

uniform vec4 ambient_color;
uniform vec4 light1_color;
uniform vec4 light1_position;
uniform vec4 light2_color;
uniform vec4 light2_position;

in vec3 position_v;
in vec3 normal_v;
in vec4 color_v;

void main ( )
{
	float t1 = max(0.0, dot(normalize(normal_v), normalize(light1_position.xyz - position_v * light1_position.w)));
	float t2 = max(0.0, dot(normalize(normal_v), normalize(light2_position.xyz - position_v * light2_position.w)));

	if (t1 + t2 > 1.0)
	{
		t1 /= (t1 + t2);
		t2 /= (t1 + t2);
	}

	gl_FragColor = (ambient_color * (1.0 - t1 - t2) + light1_color * t1 + light2_color * t2) * color_v;
}

)";

xgl::program stick_shader::make_program ( )
{
	return {
		xgl::vertex_shader(shader_vs),
		xgl::fragment_shader(shader_fs),
	};
}

void stick_shader::modelview (geom::matrix_4x4f const & transform)
{
	program_["modelview"] = transform;
}

void stick_shader::projection (geom::matrix_4x4f const & transform)
{
	program_["projection"] = transform;
}

void stick_shader::ambient_color (geom::vector_4f const & color)
{
	program_["ambient_color"] = color;
}

void stick_shader::light1_color (geom::vector_4f const & color)
{
	program_["light1_color"] = color;
}

void stick_shader::light1_position (geom::vector_4f const & position)
{
	program_["light1_position"] = position;
}

void stick_shader::light2_color (geom::vector_4f const & color)
{
	program_["light2_color"] = color;
}

void stick_shader::light2_position (geom::vector_4f const & position)
{
	program_["light2_position"] = position;
}

stick_renderer::stick_renderer (int quality)
	: count_(0)
	, instance_count_(0)
{
	set_quality(quality);
	setup_array();
}

void stick_renderer::set_quality (int quality)
{
	std::vector<geom::vector_3f> points;
	std::vector<std::uint32_t> indices;

	auto push_triangle = [&](std::size_t i1, std::size_t i2, std::size_t i3)
	{
		indices.push_back(static_cast<std::uint32_t>(i1));
		indices.push_back(static_cast<std::uint32_t>(i2));
		indices.push_back(static_cast<std::uint32_t>(i3));
	};

	auto index_at = [quality](std::size_t i, std::size_t j)
	{
		return j * (4 * quality) + (i % (4 * quality));
	};

	for (int j = 0; j < 2; ++j)
	{
		for (int i = 0; i < 4 * quality; ++i)
		{
			float const a = (i * geom::pi_f) / (2 * quality);

			geom::vector_3f p
			{
				std::cos(a),
				std::sin(a),
				static_cast<float>(j)
			};

			points.push_back(p);
		}
	}

	for (int i = 0; i < 4 * quality; ++i)
	{
		push_triangle(index_at(i, 0), index_at(i + 1, 0), index_at(i + 1, 1));
		push_triangle(index_at(i, 0), index_at(i + 1, 1), index_at(i, 1));
	}

	cylinder_.data(points);
	indices_.data(indices);

	count_ = indices.size();
}

void stick_renderer::setup_array ( )
{
	xgl::scope::bind_array autoname(array_);

	{
		xgl::scope::bind_buffer autoname(cylinder_);
		array_[0].enable().data<geom::vector_3f>(0);
		array_[1].enable().data<geom::vector_3f>(0);
	}

	{
		xgl::scope::bind_buffer autoname(sticks_);
		array_[2].enable().data<geom::vector_3f>(sizeof(float) * 0, sizeof(stick)).divisor(1);
		array_[3].enable().data<geom::vector_3f>(sizeof(float) * 3, sizeof(stick)).divisor(1);
		array_[4].enable().data<float>(sizeof(float) * 6, sizeof(stick)).divisor(1);
		array_[5].enable().data<float>(sizeof(float) * 7, sizeof(stick)).divisor(1);
		array_[6].enable().data<geom::vector_4f>(sizeof(float) * 8, sizeof(stick)).divisor(1);
		array_[7].enable().data<geom::vector_4f>(sizeof(float) * 12, sizeof(stick)).divisor(1);
	}

	array_.indices(indices_);
}

void stick_renderer::load (stick const * data, std::size_t count)
{
	sticks_.data(data, count);
	instance_count_ = count;
}

void stick_renderer::render ( )
{
	xgl::scope::bind_program autoname(shader_.program());
	xgl::scope::bind_array autoname(array_);
	glDrawElementsInstanced(GL_TRIANGLES, count_, GL_UNSIGNED_INT, nullptr, instance_count_);
}

#include <nanotubes/nanotube.hpp>

#include <geom/alias/vector.hpp>
#include <geom/operations/vector.hpp>
#include <geom/math.hpp>

geom::triangulation<geom::point_3f, 1> nanotube_graph_f (int n, int m, int l, float bond_length)
{
	geom::triangulation<geom::point_3f, 1> result;

	float const d = bond_length * std::sqrt(3.f);

	geom::vector_2f const e1 { d, 0.f };
	geom::vector_2f const e2 { d / 2.f, 3.f * bond_length / 2.f };

	geom::vector_2f const t = float(n) * e1 + float(m) * e2;

	float const L = geom::norm(t);
	float const R = L / (2.f * geom::pi_f);

	geom::vector_2f const g1 = t / L;
	geom::vector_2f const g2 = -geom::ort(g1);

	auto push_vertex = [&](geom::vector_2f p){
		float const phi = 2.f * geom::pi_f * p * g1 / L;

		float const h = p * g2;
		result.vertices.push_back({ std::cos(phi) * R, std::sin(phi) * R, h });
	};

	auto push_edge = [&](std::size_t i, std::size_t j)
	{
		result.simplices.push_back({i, j});
	};

	for (int j = 0; j < m + l; ++j)
	{
		for (int i = 0; i < n; ++i)
		{
			push_vertex(float(i) * e1 + float(j) * e2);
			push_vertex((i + 1.f / 3.f) * e1 + (j + 1.f / 3.f) * e2);

			std::size_t const base = 2 * (j * n + i);

			push_edge(base + 1, base);

			if (i + 1 < n)
				push_edge(base + 1, base + 2);
			else if (j >= m)
				push_edge(base + 1, 2 * (j - m) * n);

			if (j + 1 < l + m)
				push_edge(base + 1, 2 * ((j + 1) * n + i));
		}
	}

	return result;
}

geom::triangulation<geom::point_3d, 1> nanotube_graph_d (int n, int m, int l, double bond_length)
{
	geom::triangulation<geom::point_3d, 1> result;

	float const d = bond_length * std::sqrt(3.0);

	geom::vector_2d const e1 { d, 0.0 };
	geom::vector_2d const e2 { d / 2.0, 3.0 * bond_length / 2.0 };

	geom::vector_2d const t = double(n) * e1 + double(m) * e2;

	double const L = geom::norm(t);
	double const R = L / (2.0 * geom::pi);

	geom::vector_2d const g1 = t / L;
	geom::vector_2d const g2 = -geom::ort(g1);

	auto push_vertex = [&](geom::vector_2d p){
		double const phi = 2.0 * geom::pi * p * g1 / L;

		double const h = p * g2;
		result.vertices.push_back({ std::cos(phi) * R, std::sin(phi) * R, h });
	};

	auto push_edge = [&](std::size_t i, std::size_t j)
	{
		result.simplices.push_back({i, j});
	};

	for (int j = 0; j < m + l; ++j)
	{
		for (int i = 0; i < n; ++i)
		{
			push_vertex(double(i) * e1 + double(j) * e2);
			push_vertex((i + 1.0 / 3.0) * e1 + (j + 1.0 / 3.0) * e2);

			std::size_t const base = 2 * (j * n + i);

			push_edge(base + 1, base);

			if (i + 1 < n)
				push_edge(base + 1, base + 2);
			else if (j >= m)
				push_edge(base + 1, 2 * (j - m) * n);

			if (j + 1 < l + m)
				push_edge(base + 1, 2 * ((j + 1) * n + i));
		}
	}

	return result;
}

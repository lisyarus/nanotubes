#include <nanotubes/line_renderer.hpp>

#include <util/autoname.hpp>

static const char shader_vs[] =
R"(#version 330

uniform mat4 modelview;
uniform mat4 projection;

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 color;

out vec4 color_v;

void main ( )
{
	gl_Position = projection * modelview * position;
	color_v = color;
})";

static const char shader_fs[] =
R"(#version 330

uniform vec4 color;

out vec4 frag_color;

void main ( )
{
	frag_color = color;
}

)";


xgl::program line_shader::make_program ( )
{
	return {
		xgl::vertex_shader(shader_vs),
		xgl::fragment_shader(shader_fs),
	};
}

void line_shader::modelview (geom::matrix_4x4f const & transform)
{
	program_["modelview"] = transform;
}

void line_shader::projection (geom::matrix_4x4f const & transform)
{
	program_["projection"] = transform;
}

void line_shader::color (geom::vector_4f const & color)
{
	program_["color"] = color;
}

line_renderer::line_renderer (xgl::enum_t mode)
	: count_(0)
	, mode_(mode)
{
	setup_array();
}

void line_renderer::setup_array ( )
{
	xgl::scope::bind_array autoname(array_);

	{
		xgl::scope::bind_buffer autoname(lines_);
		array_[0].enable().data<geom::vector_3f>(0);
	}
}

void line_renderer::load (geom::point_3f const * data, std::size_t count)
{
	lines_.data(data, count);
	count_ = count;
}

void line_renderer::render ( )
{
	xgl::scope::bind_program autoname(shader_.program());
	xgl::scope::bind_array autoname(array_);
	glDrawArrays(mode_, 0, count_);
}

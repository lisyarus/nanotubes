#include <nanotubes/nanotube.hpp>
#include <nanotubes/constants.hpp>
#include <nanotubes/util.hpp>

#include <geom/operations/point.hpp>
#include <geom/math.hpp>

#include <util/to_string.hpp>

#include <iostream>
#include <fstream>

void run_generate (int /* argc */, char ** argv)
{
	int const nanotube_n = util::from_string<int>(argv[2]);
	int const nanotube_m = util::from_string<int>(argv[3]);
	int const nanotube_l = util::from_string<int>(argv[4]);

	float const box_xy = util::from_string<float>(argv[5]);
	float const box_z = util::from_string<float>(argv[6]);

	float const cell = util::from_string<float>(argv[7]);

	int const cell_count_xy = box_xy / cell;
	int const cell_count_z = box_z / cell;
	int const cell_count_total = cell_count_xy * cell_count_xy * cell_count_z;

	int const point_count_total = (cell_count_xy + 1) * (cell_count_xy + 1) * (cell_count_z + 1);

	std::string const output_path = argv[8];

	std::cout << "Generating LJ potential values for " << nanotube_n << "-" << nanotube_m << "-"
		<< nanotube_l << " nanotube\n";
	std::cout << "Inside a box " << box_xy << "x" << box_xy << "x" << box_z << "\n";
	std::cout << "Elementary cell size: " << cell << "\n";
	std::cout << "Elementary cell count: " << cell_count_xy << "x" << cell_count_xy << "x"
		<< cell_count_z << ", " << cell_count_total << " total\n";

	auto nanotube = nanotube_graph(nanotube_n, nanotube_m, nanotube_l, carbon_carbon_bond_length);
	center_z(nanotube.vertices);

	auto lj_single = [](geom::point_3f const & p1, geom::point_3f const & p2)
	{
		float const r_2 = geom::distance_sqr(p1, p2);
		float const sigma_over_r_2 = (carbon_carbon_lj_sigma * carbon_carbon_lj_sigma) / r_2;

		float const sigma_over_r_6 = sigma_over_r_2 * sigma_over_r_2 * sigma_over_r_2;

		return 4.f * carbon_carbon_lj_epsilon * (sigma_over_r_6 * sigma_over_r_6 - sigma_over_r_6);
	};

	auto lj = [&nanotube, lj_single](geom::point_3f const & p)
	{
		float sum = 0.f;
		for (auto const & q : nanotube.vertices)
		{
			sum += lj_single(p, q);
		}
		return sum;
	};

	std::ofstream output(output_path, std::ios_base::binary);

	auto write = [&output](auto const & x)
	{
		output.write(reinterpret_cast<char const *>(&x), sizeof(x));
	};

	write(nanotube_n);
	write(nanotube_m);
	write(nanotube_l);

	write(box_xy);
	write(box_z);

	write(cell_count_xy);
	write(cell_count_z);

	float min_value = std::numeric_limits<float>::infinity();
	float min_distance = 0.f;

	for (int x = 0; x <= cell_count_xy; ++x)
	{
		for (int y = 0; y <= cell_count_xy; ++y)
		{
			for (int z = 0; z <= cell_count_z; ++z)
			{
				geom::point_3f p;
				p[0] = - 0.5f * box_xy + (x * box_xy) / cell_count_xy;
				p[1] = - 0.5f * box_xy + (y * box_xy) / cell_count_xy;
				p[2] = - 0.5f * box_z  + (z * box_z ) / cell_count_z ;

				float const value = lj(p);
				write(value);

				if (value < min_value)
				{
					min_value = value;
					min_distance = std::sqrt(geom::sqr(p[0]) + geom::sqr(p[1]));
				}
			}
		}

		int const point_count_ready = (x + 1) * (cell_count_xy + 1) * (cell_count_z + 1);

		std::cout << point_count_ready << "/" << point_count_total << " ("
			<< (100.f * static_cast<float>(point_count_ready) / point_count_total) << "%)" << std::endl;
	}

	std::cout << "Approx minima axis distance: " << min_distance << '\n';
}

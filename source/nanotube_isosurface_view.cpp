#include <nanotubes/util.hpp>
#include <nanotubes/nanotube.hpp>
#include <nanotubes/constants.hpp>
#include <nanotubes/application.hpp>
#include <nanotubes/nanotube_renderer.hpp>
#include <nanotubes/line_renderer.hpp>
#include <nanotubes/mesh_renderer.hpp>

#include <xgl/state/enable.hpp>
#include <xgl/color.hpp>
#include <xgl/mesh/mesh.hpp>

#include <geom/operations/vector.hpp>

#include <util/to_string.hpp>
#include <util/autoname.hpp>
#include <util/threadpool.hpp>

#include <fstream>
#include <iostream>
#include <vector>

struct binary_input_file
{
	binary_input_file (std::string const & path)
		: f(path, std::ios_base::binary)
	{ }

	template <typename T>
	T read ( )
	{
		T x;
		f.read(reinterpret_cast<char *>(&x), sizeof(x));
		return x;
	}

	template <typename T>
	void read (T * p, std::size_t count)
	{
		f.read(reinterpret_cast<char *>(p), sizeof(T) * count);
	}

private:
	std::ifstream f;
};

std::vector<mesh_point> fill_normals (std::vector<geom::point_3f> const & t)
{
	std::vector<mesh_point> result;
	result.reserve(t.size());

	for (std::size_t i = 0; i < t.size(); i += 3)
	{
		auto p0 = t[i + 0];
		auto p1 = t[i + 1];
		auto p2 = t[i + 2];

		auto n = normalized((p2 - p0) ^ (p1 - p0));

		result.push_back({p0, n});
		result.push_back({p1, n});
		result.push_back({p2, n});
	}

	return result;
}

std::vector<geom::point_3f> get_partial_isosurface (std::vector<float> const & values, float target_value,
	geom::vector_3i dim, geom::vector_3i base, geom::vector_3i size, geom::rectangle_3f box)
{
	static constexpr std::array<geom::vector_3i, 4> tetrahedra[6] = {
		{{ {0,0,0}, {1,0,0}, {1,1,0}, {1,1,1} }},
		{{ {0,0,0}, {1,1,0}, {0,1,0}, {1,1,1} }},
		{{ {0,0,0}, {0,1,0}, {0,1,1}, {1,1,1} }},
		{{ {0,0,0}, {0,1,1}, {0,0,1}, {1,1,1} }},
		{{ {0,0,0}, {0,0,1}, {1,0,1}, {1,1,1} }},
		{{ {0,0,0}, {1,0,1}, {1,0,0}, {1,1,1} }},
	};

	geom::triangulation<geom::point_3f, 3> grid;

	auto point_at = [&](int x, int y, int z)
	{
		geom::point_3f p;
		p[0] = box[0].inf + box[0].length() * (x + base[0]) / dim[0];
		p[1] = box[1].inf + box[1].length() * (y + base[1]) / dim[1];
		p[2] = box[2].inf + box[2].length() * (z + base[2]) / dim[2];
		return p;
	};

	auto index_at = [&](int x, int y, int z)
	{
		std::size_t gx = x + base[0];
		std::size_t gy = y + base[1];
		std::size_t gz = z + base[2];

		return gz + (dim[2] + 1) * (gy + (dim[1] + 1) * gx);
	};

	std::vector<geom::point_3f> result;

	auto push = [&result](geom::point_3f p0, geom::point_3f p1, geom::point_3f p2){
		result.push_back(p0);
		result.push_back(p1);
		result.push_back(p2);
	};

	for (int x = 0; x < size[0]; ++x)
	{
		for (int y = 0; y < size[1]; ++y)
		{
			for (int z = 0; z < size[2]; ++z)
			{
				for (auto const & t : tetrahedra)
				{
					auto p = [&](int i0, int i1)
					{
						auto const & p0 = point_at(x + t[i0][0], y + t[i0][1], z + t[i0][2]);
						auto const & p1 = point_at(x + t[i1][0], y + t[i1][1], z + t[i1][2]);

						auto v0 = values[index_at(x + t[i0][0], y + t[i0][1], z + t[i0][2])] - target_value;
						auto v1 = values[index_at(x + t[i1][0], y + t[i1][1], z + t[i1][2])] - target_value;

						return p0 - (p1 - p0) * v0 / (v1 - v0);
					};

					int mask = 0;
					for (std::size_t i = 0; i < 4; ++i)
					{
						mask |= (values[index_at(x + t[i][0], y + t[i][1], z + t[i][2])] > target_value) ? (1 << i) : 0;
					}

					switch (mask)
					{
					case 0b0000:
						break;
					case 0b0001:
						push(p(0,1), p(0,2), p(0,3));
						break;
					case 0b0010:
						push(p(0,1), p(1,3), p(1,2));
						break;
					case 0b0011:
						push(p(0,2), p(0,3), p(1,3));
						push(p(0,2), p(1,3), p(1,2));
						break;
					case 0b0100:
						push(p(0,2), p(1,2), p(2,3));
						break;
					case 0b0101:
						push(p(2,3), p(0,3), p(0,1));
						push(p(2,3), p(0,1), p(1,2));
						break;
					case 0b0110:
						push(p(0,1), p(1,3), p(2,3));
						push(p(0,1), p(2,3), p(0,2));
						break;
					case 0b0111:
						push(p(0,3), p(1,3), p(2,3));
						break;
					case 0b1000:
						push(p(2,3), p(1,3), p(0,3));
						break;
					case 0b1001:
						push(p(0,2), p(2,3), p(0,1));
						push(p(2,3), p(1,3), p(0,1));
						break;
					case 0b1010:
						push(p(1,2), p(0,1), p(2,3));
						push(p(0,1), p(0,3), p(2,3));
						break;
					case 0b1011:
						push(p(2,3), p(1,2), p(0,2));
						break;
					case 0b1100:
						push(p(1,2), p(1,3), p(0,2));
						push(p(1,3), p(0,3), p(0,2));
						break;
					case 0b1101:
						push(p(1,2), p(1,3), p(0,1));
						break;
					case 0b1110:
						push(p(0,3), p(0,2), p(0,1));
						break;
					case 0b1111:
						break;
					}
				}
			}
		}
	}

	return result;
}

void run_view (int argc, char ** argv)
{
	std::string const input_path = argv[2];
	std::vector<float> isosurface_values;
	for (int a = 3; a < argc; ++a)
		isosurface_values.push_back(util::from_string<float>(argv[a]));

	binary_input_file input(input_path);

	int const nanotube_n = input.read<int>();
	int const nanotube_m = input.read<int>();
	int const nanotube_l = input.read<int>();

	float const box_xy = input.read<float>();
	float const box_z = input.read<float>();

	geom::rectangle_3f const box = {{
		{-0.5f * box_xy, 0.5f * box_xy},
		{-0.5f * box_xy, 0.5f * box_xy},
		{-0.5f * box_z, 0.5f * box_z},
	}};

	int const cell_count_xy = input.read<int>();
	int const cell_count_z = input.read<int>();

	std::cout << "Viewing nanotube LJ potential isosurface\n";
	std::cout << "Nanotube: " << nanotube_n << "-" << nanotube_m << "-"
		<< nanotube_l << "\n";
	std::cout << "Box: " << box_xy << "x" << box_xy << "x" << box_z << "\n";
	std::cout << "Cell count: " << cell_count_xy << "x" << cell_count_xy << "x"
		<< cell_count_z << "\n";
	std::cout << "Isosurface values: ";
	for (float v : isosurface_values)
		std::cout << v << ' ';
	std::cout << '\n';

	std::cout << "Reading input values..." << std::flush;
	std::vector<float> lj_values((cell_count_xy + 1) * (cell_count_xy + 1) * (cell_count_z + 1));
	input.read(lj_values.data(), lj_values.size());
	std::cout << "done\n";

	{
		float min =  std::numeric_limits<float>::infinity();
		float max = -std::numeric_limits<float>::infinity();

		std::cout << "Potential value range: " << std::flush;

		for (float f : lj_values)
		{
			if (min > f)
				min = f;
			if (max < f)
				max = f;
		}

		std::cout << "[" << min << " .. " << max << "]" << std::endl;
	}

	std::vector<geom::vector_4f> isosurface_colors;
	while (isosurface_colors.size() < isosurface_values.size())
	{
		isosurface_colors.push_back(xgl::color::red);
		isosurface_colors.push_back(xgl::color::green);
		isosurface_colors.push_back(xgl::color::blue);
		isosurface_colors.push_back(xgl::color::cyan);
		isosurface_colors.push_back(xgl::color::magenta);
		isosurface_colors.push_back(xgl::color::yellow);
	}

	auto nanotube = nanotube_graph(nanotube_n, nanotube_m, nanotube_l, carbon_carbon_bond_length);
	center_z(nanotube.vertices);

	application app;

	nanotube_renderer nr(12);
	nr.load(nanotube);

	line_renderer lr;
	lr.shader().color(xgl::color::red);
	{
		std::vector<geom::point_3f> lines;

		float const a = box_xy / 2.f;
		float const b = box_z / 2.f;

		// X

		lines.push_back({-a, -a, -b});
		lines.push_back({ a, -a, -b});

		lines.push_back({-a,  a, -b});
		lines.push_back({ a,  a, -b});

		lines.push_back({-a, -a,  b});
		lines.push_back({ a, -a,  b});

		lines.push_back({-a,  a,  b});
		lines.push_back({ a,  a,  b});

		// Y

		lines.push_back({-a, -a, -b});
		lines.push_back({-a,  a, -b});

		lines.push_back({ a, -a, -b});
		lines.push_back({ a,  a, -b});

		lines.push_back({-a, -a,  b});
		lines.push_back({-a,  a,  b});

		lines.push_back({ a, -a,  b});
		lines.push_back({ a,  a,  b});

		// Z

		lines.push_back({-a, -a, -b});
		lines.push_back({-a, -a,  b});

		lines.push_back({ a, -a, -b});
		lines.push_back({ a, -a,  b});

		lines.push_back({-a,  a, -b});
		lines.push_back({-a,  a,  b});

		lines.push_back({ a,  a, -b});
		lines.push_back({ a,  a,  b});

		lr.load(lines);
	}

	std::vector<mesh_renderer> isosurfaces;

	util::threadpool pool;

	std::vector<std::pair<std::size_t, std::vector<mesh_point>>> ready_isosurfaces;
	std::mutex ready_isosurfaces_mutex;

	int const task_size = 100;

	int tasks_total = 0;

	float sum_distance = 0.f;
	int point_count = 0;

	for (std::size_t v = 0; v < isosurface_values.size(); ++v)
	{
		for (int x = 0; x < cell_count_xy; x += task_size)
		{
			for (int y = 0; y < cell_count_xy; y += task_size)
			{
				for (int z = 0; z < cell_count_z; z += task_size)
				{
					pool.dispatch([&, v, x, y, z]{

						geom::vector_3i const size =
						{
							std::min(task_size, cell_count_xy - x),
							std::min(task_size, cell_count_xy - y),
							std::min(task_size, cell_count_z - z),
						};

						auto points = get_partial_isosurface(lj_values, isosurface_values[v], {cell_count_xy, cell_count_xy, cell_count_z},
						{x, y, z}, size, box);
						auto mesh_points = fill_normals(points);

						std::lock_guard<std::mutex> lock(ready_isosurfaces_mutex);
						ready_isosurfaces.push_back({ v, std::move(mesh_points) });

					});
					++tasks_total;
				}
			}
		}
	}

	xgl::enable(GL_CULL_FACE, false);

	app.run([&]
	{
		{
			std::lock_guard<std::mutex> lock(ready_isosurfaces_mutex);

			for (auto const & iso : ready_isosurfaces)
			{
				isosurfaces.emplace_back();
				isosurfaces.back().shader().mesh_color(isosurface_colors[iso.first]);
				isosurfaces.back().load(iso.second);

				for (mesh_point const & p : iso.second)
				{
					sum_distance += std::sqrt(geom::sqr(p.position[0]) + geom::sqr(p.position[1]));
					point_count += 1;
				}
			}

			ready_isosurfaces.clear();
		}


		if (isosurfaces.size() == tasks_total && point_count > 0)
		{
			std::cout << "All isosurfaces constructed\n";
			std::cout << "Average axis distance: " << (sum_distance / point_count) << "\n";
			point_count = 0;
		}

		glClearColor(0.7f, 0.8f, 0.9f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		{
			xgl::scope::enable autoname(GL_DEPTH_TEST);

			{
				xgl::scope::enable autoname(GL_CULL_FACE);

				app.render(nr.balls());
				app.render(nr.sticks());
				app.render(lr);
			}

			for (auto & iso : isosurfaces)
				app.render(iso);
		}
	});

	pool.stop();
}

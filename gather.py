#!/usr/bin/env python3

from multiprocessing import Pool
from subprocess import check_output
import os
import functools
import uuid


polyethylene_length = 20
nanotube_length = 30
nanotube_n_range = list(range(7,16))
runs = 50


def process(args):
    n = args[0]
    m = args[1]
    result = check_output(['release/nanotube_md_nogui', 'bound', str(polyethylene_length), str(n), str(m), str(nanotube_length), '1']).decode('utf-8')
    path = 'data/{}-{}/'.format(n,m)
    filename = uuid.uuid4().hex
    if not os.path.exists(path):
        os.makedirs(path)
    with open(os.path.join(path, filename), 'wt') as f:
        f.write(result)


def main():
    pool = Pool(processes=4)

    args = [(n,m) for n in nanotube_n_range for m in range(n+1)]
    args = args * runs

    pool.map(process, args)


if __name__ == '__main__':
    main()

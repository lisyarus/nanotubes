#pragma once

#include <nanotubes/shader_base.hpp>

#include <xgl/objects/array.hpp>
#include <xgl/objects/buffer.hpp>

struct mesh_point
{
	geom::point_3f position;
	geom::vector_3f normal;
};

struct mesh_shader
	: shader_base<mesh_shader>
{
	static xgl::program make_program ( );

	void modelview (geom::matrix_4x4f const & transform);
	void projection (geom::matrix_4x4f const & transform);
	void ambient_color (geom::vector_4f const & color);
	void light1_color (geom::vector_4f const & color);
	void light1_position (geom::vector_4f const & position);
	void light2_color (geom::vector_4f const & color);
	void light2_position (geom::vector_4f const & position);

	void mesh_color (geom::vector_4f const & color);
};

struct mesh_renderer
{
	mesh_renderer ( );

	void load (mesh_point const * data, std::size_t count);

	template <typename Array>
	void load (Array const & array)
	{
		load(array.data(), array.size());
	}

	auto & shader ( )
	{
		return shader_;
	}

	void render ( );

private:
	mesh_shader shader_;
	xgl::array array_;
	xgl::array_buffer mesh_;

	int count_;

	void setup_array();
};

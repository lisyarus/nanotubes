#pragma once

#include <nanotubes/shader_base.hpp>

#include <xgl/objects/array.hpp>
#include <xgl/objects/buffer.hpp>

struct stick
{
	geom::point_3f position[2];
	float radius[2];
	geom::vector_4f color[2];
};

struct stick_shader
	: shader_base<stick_shader>
{
	static xgl::program make_program ( );

	void modelview (geom::matrix_4x4f const & transform);
	void projection (geom::matrix_4x4f const & transform);
	void ambient_color (geom::vector_4f const & color);
	void light1_color (geom::vector_4f const & color);
	void light1_position (geom::vector_4f const & position);
	void light2_color (geom::vector_4f const & color);
	void light2_position (geom::vector_4f const & position);
};

struct stick_renderer
{
	stick_renderer (int quality = 6);

	void set_quality (int quality);

	void load (stick const * data, std::size_t count);

	template <typename Array>
	void load (Array const & array)
	{
		load(array.data(), array.size());
	}

	stick_shader & shader ( )
	{
		return shader_;
	}

	void render ( );

private:
	stick_shader shader_;
	xgl::array array_;
	xgl::array_buffer cylinder_;
	xgl::array_buffer indices_;
	xgl::array_buffer sticks_;

	int count_, instance_count_;

	void setup_array();
};

#pragma once

#include <nanotubes/color.hpp>
#include <nanotubes/shader_base.hpp>

#include <xgl/objects/array.hpp>
#include <xgl/objects/buffer.hpp>


struct line_shader
	: shader_base<line_shader>
{
	static xgl::program make_program ( );

	void modelview (geom::matrix_4x4f const & transform);
	void projection (geom::matrix_4x4f const & transform);

	void color (geom::vector_4f const & color);
};

struct line_renderer
{
	line_renderer (xgl::enum_t mode = GL_LINES);

	void load (geom::point_3f const * data, std::size_t count);

	template <typename Array>
	void load (Array const & array)
	{
		load(array.data(), array.size());
	}

	auto & shader ( )
	{
		return shader_;
	}

	void render ( );

private:
	line_shader shader_;
	xgl::array array_;
	xgl::array_buffer lines_;
	xgl::enum_t mode_;

	int count_;

	void setup_array();
};

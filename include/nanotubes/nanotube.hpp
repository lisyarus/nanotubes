#pragma once

#include <geom/primitives/triangulation.hpp>
#include <geom/alias/point.hpp>

geom::triangulation<geom::point_3f, 1> nanotube_graph_f (int n, int m, int l, float bond_length);
geom::triangulation<geom::point_3d, 1> nanotube_graph_d (int n, int m, int l, double bond_length);

template <typename Scalar>
geom::triangulation<geom::point_3<Scalar>, 1> nanotube_graph (int n, int m, int l, Scalar bond_length)
{
	if constexpr (std::is_same_v<Scalar, float>)
	{
		return nanotube_graph_f(n, m, l, bond_length);
	}
	else if constexpr (std::is_same_v<Scalar, double>)
	{
		return nanotube_graph_d(n, m, l, bond_length);
	}
	else
	{
		static_assert("WTF");
	}
}

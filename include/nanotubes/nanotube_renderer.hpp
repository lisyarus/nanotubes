#pragma once

#include <nanotubes/color.hpp>
#include <nanotubes/ball_renderer.hpp>
#include <nanotubes/stick_renderer.hpp>

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>
#include <geom/primitives/triangulation.hpp>

#include <vector>

struct nanotube_renderer
{
	static auto constexpr nanotube_color = color { 0.6f, 0.6f, 0.6f, 1.f };
	static auto constexpr nanotube_ball_radius = 0.2f;
	static auto constexpr nanotube_stick_radius = 0.1f;

	nanotube_renderer (int quality = 6)
		: br(quality)
		, sr(quality)
	{ }

	void load (geom::triangulation<geom::point_3f, 1> const & t, color const & c = nanotube_color)
	{
		{
			std::vector<ball> nanotube_balls;
			for (auto const & p : t.vertices)
			{
				nanotube_balls.push_back({p, nanotube_ball_radius, c});
			}
			br.load(nanotube_balls);
		}

		{
			std::vector<stick> nanotube_sticks;
			for (auto const & e : t.simplices)
			{
				nanotube_sticks.push_back({
					{ t.vertices[e[0]], t.vertices[e[1]] },
					{ nanotube_stick_radius, nanotube_stick_radius },
					{ c, c }
				});
			}
			sr.load(nanotube_sticks);
		}
	}

	auto & balls ( )
	{
		return br;
	}

	auto & sticks ( )
	{
		return sr;
	}

private:
	ball_renderer br;
	stick_renderer sr;
};

#pragma once

constexpr float carbon_carbon_bond_length = 1.44f; // angstrom

constexpr float carbon_carbon_lj_sigma = 3.37f; // angstrom
constexpr float carbon_carbon_lj_epsilon = 4.2038e-3f; // eV

constexpr float polyethylene_kr = 13.743f; // eV / angstrom^2
constexpr float polyethylene_r0 = 1.533f; // angstrom

constexpr float polyethylene_ktheta = 1.5852e-6; // eV / rad^2
constexpr float polyethylene_theta0 = 1.9106f; // rad

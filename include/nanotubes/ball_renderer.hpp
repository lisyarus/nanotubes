#pragma once

#include <nanotubes/shader_base.hpp>

#include <xgl/objects/array.hpp>
#include <xgl/objects/buffer.hpp>

struct ball
{
	geom::point_3f position;
	float radius;
	geom::vector_4f color;
};

struct ball_shader
	: shader_base<ball_shader>
{
	static xgl::program make_program ( );

	void modelview (geom::matrix_4x4f const & transform);
	void projection (geom::matrix_4x4f const & transform);
	void ambient_color (geom::vector_4f const & color);
	void light1_color (geom::vector_4f const & color);
	void light1_position (geom::vector_4f const & position);
	void light2_color (geom::vector_4f const & color);
	void light2_position (geom::vector_4f const & position);
};

struct ball_renderer
{
	ball_renderer (int quality = 6);

	void set_quality (int quality);

	void load (ball const * data, std::size_t count);

	template <typename Array>
	void load (Array const & array)
	{
		load(array.data(), array.size());
	}

	ball_shader & shader ( )
	{
		return shader_;
	}

	void render ( );

private:
	ball_shader shader_;
	xgl::array array_;
	xgl::array_buffer sphere_;
	xgl::array_buffer indices_;
	xgl::array_buffer balls_;

	int count_, instance_count_;

	void setup_array();
};

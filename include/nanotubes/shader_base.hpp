#pragma once

#include <xgl/shaders/base.hpp>

template <typename Derived>
struct shader_base
	: xgl::shaders::base
{
	shader_base ( )
		: xgl::shaders::base(Derived::make_program())
	{ }

	void modelview (geom::matrix_4x4f const & transform) {}
	void projection (geom::matrix_4x4f const & transform) {}
	void ambient_color (geom::vector_4f const & color) {}
	void light1_color (geom::vector_4f const & color) {}
	void light1_position (geom::vector_4f const & position) {}
	void light2_color (geom::vector_4f const & color) {}
	void light2_position (geom::vector_4f const & position) {}
};

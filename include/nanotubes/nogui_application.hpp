#pragma once

#include <variant>

#include <iostream>

#include <geom/alias/vector.hpp>

struct nogui_application
{
	struct mock_shader
	{
		void color (geom::vector_4f)
		{ }
	};

	struct mock_renderer
	{
		template <typename ... Args>
		mock_renderer (Args const & ...)
		{ }

		template <typename ... Args>
		void load (Args const & ...)
		{ }

		std::monostate balls ( )
		{ return {}; }

		std::monostate sticks ( )
		{ return {}; }

		mock_shader shader ( )
		{ return {}; }
	};

	struct mock_stream
	{
		template <typename T>
		mock_stream & operator << (T const &)
		{
			return *this;
		}
	};

	using nanotube_renderer = mock_renderer;
	using line_renderer = mock_renderer;

	nogui_application ( )
		: running(false)
	{ }

	mock_stream debug_ ( )
	{
		return {};
	}

	auto & debug ( )
	{
		return std::cout;
	}

	void stop ( )
	{
		running = false;
	}

	template <typename PaintCallback>
	void run (PaintCallback && paint)
	{
		running = true;

		while (running)
		{
			if (!running) break;

			paint();
		}
	}

	template <typename Renderable>
	void render (Renderable const & r)
	{ }

	bool running;
};

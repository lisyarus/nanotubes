#pragma once

#include <geom/alias/vector.hpp>
#include <geom/alias/matrix.hpp>

namespace xsdl
{

	struct event_poller_t;

}

struct camera
{
	float a, b;
	float d;

	camera ( );

	geom::matrix_4x4f matrix ( ) const;

	geom::vector_3f direction ( ) const;
};

void setup_controls (camera & cam, xsdl::event_poller_t & event_poller);

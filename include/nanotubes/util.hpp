#pragma once

#include <geom/alias/point.hpp>
#include <vector>

template <typename Scalar>
void center_z (std::vector<geom::point_3<Scalar>> & points)
{
	Scalar zmin = std::numeric_limits<Scalar>::infinity();
	Scalar zmax = -std::numeric_limits<Scalar>::infinity();

	for (auto const & p : points)
	{
		if (p[2] < zmin)
			zmin = p[2];

		if (p[2] > zmax)
			zmax = p[2];
	}

	Scalar const zc = (zmin + zmax) / 2;

	for (auto & p : points)
	{
		p[2] -= zc;
	}
}

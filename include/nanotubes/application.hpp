#pragma once

#include <nanotubes/camera.hpp>
#include <nanotubes/color.hpp>
#include <nanotubes/nanotube_renderer.hpp>
#include <nanotubes/line_renderer.hpp>

#include <xsdl/init.hpp>
#include <xsdl/event_poller.hpp>
#include <xsdl/opengl.hpp>

#include <SDL2/SDL.h>

#include <xgl/context.hpp>
#include <xgl/state/viewport.hpp>
#include <xgl/state/enable.hpp>

#include <geom/math.hpp>
#include <geom/transform/perspective.hpp>

#include <util/autoname.hpp>

#include <iostream>

struct application
{
	using nanotube_renderer = ::nanotube_renderer;
	using line_renderer = ::line_renderer;

	application ( )
		: window(event_poller, "Nanotubes", {800, 600}, {3, 3, false}, 2)
		, xgl_context(xgl::make_global_context(xgl::make_cached_state()))
		, projection(geom::matrix_4x4f::identity())
		, running(false)
	{
		xgl_context->make_current();

		event_poller.on_quit([this]{ running = false; });

		event_poller.on_resize([this](xsdl::window_t::id_t, xsdl::window_t::size_t size){
			float aspect_ratio = static_cast<float>(size.width) / size.height;
			projection = geom::perspective<float, 3>(geom::pi / 3.f, aspect_ratio, 0.01f, 10000.f).homogeneous_matrix();

			xgl::viewport({{{0, size.width}, {0, size.height}}});
		});

		event_poller.on_key_down([this](xsdl::window_t::id_t, std::int32_t key){
			switch (key)
			{
			case SDLK_ESCAPE:
				running = false;
				break;
			}
		});

		setup_controls(cam, event_poller);
	}

	std::ostream & debug ( )
	{
		return std::cout;
	}

	void stop ( )
	{ }

	template <typename PaintCallback>
	void run (PaintCallback && paint)
	{
		running = true;

		window.show();

		while (running)
		{
			event_poller.poll();

			if (!running) break;

			{
				glClearColor(0.7f, 0.8f, 0.9f, 0.f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				xgl::scope::enable autoname(GL_DEPTH_TEST);

				paint();
			}

			window.swap_buffers();
		}
	}

	template <typename Shader>
	void setup_shader (Shader & shader)
	{
		shader.modelview(geom::matrix_4x4f::identity());
		shader.projection(projection * cam.matrix());
		shader.ambient_color(ambient_color);
		shader.light1_color(light1_color);
		shader.light2_color(light2_color);
		shader.light1_position(light1_position);
		shader.light2_position(light2_position);
	}

	template <typename Renderable>
	void render (Renderable & r)
	{
		setup_shader(r.shader());
		r.render();
	}

	xsdl::initializer_t xsdl_init;
	xsdl::event_poller_t event_poller;
	xsdl::gl_window_t window;

	std::unique_ptr<xgl::context> xgl_context;

	geom::matrix_4x4f projection;

	camera cam;

	bool running;
};

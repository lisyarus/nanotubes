#pragma once

#include <geom/alias/vector.hpp>

using color = geom::vector_4f;

static auto constexpr ambient_color = color {0.0f, 0.0f, 0.0f, 1.f};
static auto constexpr light1_color = color {0.9f, 0.9f, 1.f, 1.f};
static auto constexpr light2_color = color {1.f, 1.f, 0.9f, 1.f};
static auto constexpr light1_position = geom::vector_4f {10.f, 8.f, 6.f, 0.f};
static auto constexpr light2_position = geom::vector_4f {-10.f, -8.f, -6.f, 0.f};

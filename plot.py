#!/usr/bin/env python3

import matplotlib.pyplot as plt
import sys

r_list = []
dm_list = []
approx_list = []

k_list = []
k_target = 2.95

def approx(r):
    return 7.5 / (r**3) + 2.12

with open(sys.argv[1]) as f:
    for line in f:
       n, m, r, rm = map(float, line.split(' '))
       n = int(n)
       m = int(m)
       r_list.append(r)
       dm = r - rm
       dm_list.append(dm)
       approx_list.append(approx(r))

       if rm > 0:
          k = r / rm
          k_list.append(((n,m), k))

k_list = list(sorted(k_list, key=lambda x: abs(x[1]-k_target)))
print(k_list)

plt.grid()
plt.plot(r_list, dm_list, 'bo')
#plt.plot(r_list, approx_list, 'ro')
plt.show()
